<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            "name" => "Administrador",
            "email" => "correo@correo.com",
            "email_verified_at" => Carbon::now(),
            "password" => Hash::make("Informatica_1"),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        $user = User::create([
            "name" => "Dani",
            "email" => "dani@correo.com",
            "email_verified_at" => Carbon::now(),
            "password" => Hash::make("Informatica_1"),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }
}
