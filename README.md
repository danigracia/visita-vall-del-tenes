<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!-- <a href="https://github.com/danigracia/visita-vall-del-tenes">
    <img src="public/favicon.ico" alt="Logo" width="80" height="80">
  </a> -->

<h1 align="center" >Vistia la Vall del Tenes</h1>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Tabla de Contenidos</summary>
  <ol>
    <li>
      <a href="#about-the-project">Sobre el Proyecto</a>
      <ul>
        <li><a href="#built-with">Hecho En</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisitos</a></li>
        <li><a href="#installation">Instalación</a></li>
      </ul>
    </li>
    <li><a href="#license">Licencia</a></li>
    <li><a href="#contact">Contacto</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## Sobre el Proyecto

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

### Hecho En

Frontend
* [Vue.js](https://vuejs.org/)
* [Bootstrap](https://getbootstrap.com)

Backend
* [Laravel](https://laravel.com)
* [MySQL](https://www.mysql.com/)




<!-- GETTING STARTED -->
## Getting Started

Para tener una copia local funcionando, por favor sigue los siguientes pasos.

### Prerequisitos

Para poder usar el repositorio es necesario tener instalado lo siguiente

* PHP 7 or Higher

* MySQL

* npm
  ```sh
  npm install npm@latest -g
  ```

### Instalación

1. Clonar el repositorio
   ```sh
   git clone https://gitlab.com/danigracia/visita-vall-del-tenes.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Rename [.env-example](https://gitlab.com/danigracia/visita-vall-del-tenes/-/blob/main/.env.example) to .env

4. Remplaza con tus datos
   ```env
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=laravel
   DB_USERNAME=root
   DB_PASSWORD=

   MAIL_MAILER=smtp
   MAIL_HOST=smtp.mailtrap.io
   MAIL_PORT=2525
   MAIL_USERNAME=null
   MAIL_PASSWORD=null
   MAIL_ENCRYPTION=null
   MAIL_FROM_ADDRESS=null
   MAIL_FROM_NAME="${APP_NAME}"
   ```
5. Cargar Migraciones y Seeders
   ```sh
   php artisan migrate
   php artisan db:seed
   ```
6. Serve the project
   ```sh
   php artisan serve
   ```



<!-- LICENSE -->
## Licencia

Distribuido bajo licencia GPL. Vea `LICENSE.txt` para mas información.



<!-- CONTACT -->
## Contacto

Dani - danigraciaquiroga@gmail.com

Enlace del Proyecto: [https://gitlab.com/danigracia/visita-vall-del-tenes](https://gitlab.com/danigracia/visita-vall-del-tenes)


<p align="right">(<a href="#top">volver al inicio</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
[product-screenshot]: imagenes/screenshot.png
