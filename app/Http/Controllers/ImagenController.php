<?php

namespace App\Http\Controllers;

use App\Imagen;
use App\Establecimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImagenController extends Controller
{

    public function store(Request $request)
    {
        //Read image
        $ruta_imagen = $request->file("file")->store("establecimientos", "public");

        //Resize imagen
        $imagen = Image::make( public_path("storage/{$ruta_imagen}") )->fit(800, 450);
        $imagen->save();

        //Almacenar modelo
        $imagenDB = new Imagen();
        $imagenDB->id_establecimiento = $request["uuid"];
        $imagenDB->ruta_imagen = $ruta_imagen;
        $imagenDB->save();

        //Retornar respuesta
        $respuesta = [
            "archivo" => $ruta_imagen
        ];

        return response()->json($respuesta);
    }

    public function destroy(Request $request)
    {
        //Validación
        $establecimiento = Establecimiento::where("uuid", $request->get("uuid"))->first();
        $this->authorize("delete", $establecimiento);

        //Eliminar imagen
        $imagen = $request->get("imagen");

        if(File::exists("storage/" . $imagen)) {
            //Elimina imagen del servidor
            File::delete("storage/" . $imagen);

            //Elimina imagen de la BD
            Imagen::where("ruta_imagen", $imagen)->delete();

            $respuesta = [
                "mensaje" => "Imagen Eliminada"
            ];
        }

        return response()->json($respuesta);
    }
}
