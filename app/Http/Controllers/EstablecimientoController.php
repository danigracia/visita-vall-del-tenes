<?php

namespace App\Http\Controllers;

use App\Imagen;
use App\Categoria;
use App\Establecimiento;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class EstablecimientoController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Consultar categorias
        $categorias = Categoria::all();

        return view("establecimientos.create", compact("categorias"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "nombre" => "required",
            "categoria_id" => "required|exists:App\Categoria,id",
            "imagen_principal" => "required|image|max:3000",
            "direccion" => "required",
            "localidad" => "required",
            "lat" => "required",
            "lng" => "required",
            "telefono" => "required|numeric",
            "descripcion" => "required|min:50",
            "apertura" => "date_format:H:i",
            "cierre" => "date_format:H:i",
            "uuid" => "required|uuid"
        ]);

        //Guardar la imagen
        $ruta_imagen = $request->file("imagen_principal")->store("principales", "public");
        $imagen = Image::make( public_path("storage/{$ruta_imagen}") )->fit(800, 600);
        $imagen->save();

        //Guardar DB
        $establecimiento = new Establecimiento($data);
        $establecimiento->imagen_principal = $ruta_imagen;
        $establecimiento->user_id = auth()->user()->id;
        $establecimiento->save();

        return back()->with("mensaje", "La teva informació s'ha guardat correctament");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Establecimiento $establecimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Establecimiento $establecimiento)
    {
        //Consultar categorias
        $categorias = Categoria::all();

        //Obtener establecimiento
        $establecimiento = auth()->user()->establecimiento;
        $establecimiento->apertura = date("H:i", strtotime($establecimiento->apertura));
        $establecimiento->cierre = date("H:i", strtotime($establecimiento->cierre));

        //Obtiene imagenes
        $imagenes = Imagen::where("id_establecimiento", $establecimiento->uuid)->get();

        return view("establecimientos.edit", compact("categorias", "establecimiento", "imagenes"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Establecimiento $establecimiento)
    {
        $this->authorize("update", $establecimiento);

        $data = $request->validate([
            "nombre" => "required",
            "categoria_id" => "required|exists:App\Categoria,id",
            "imagen_principal" => "image|max:3000",
            "direccion" => "required",
            "localidad" => "required",
            "lat" => "required",
            "lng" => "required",
            "telefono" => "required|numeric",
            "descripcion" => "required|min:50",
            "apertura" => "date_format:H:i",
            "cierre" => "date_format:H:i",
            "uuid" => "required|uuid"
        ]);

        $establecimiento->nombre = $data["nombre"];
        $establecimiento->categoria_id = $data["categoria_id"];
        $establecimiento->direccion = $data["direccion"];
        $establecimiento->lat = $data["lat"];
        $establecimiento->lng = $data["lng"];
        $establecimiento->telefono = $data["telefono"];
        $establecimiento->descripcion = $data["descripcion"];
        $establecimiento->apertura = $data["apertura"];
        $establecimiento->cierre = $data["cierre"];
        $establecimiento->uuid = $data["uuid"];

        if(request("imagen_principal")){
            //Guardar
            $ruta_imagen = $request->file("imagen_principal")->store("principales", "public");

            //Resize
            $imagen = Image::make( public_path("storage/{$ruta_imagen}") )->fit(800, 600);
            $imagen->save();

            $establecimiento->imagen_principal = $ruta_imagen;
        }

        $establecimiento->save();

        return back()->with("mensaje", "La teva informació s'ha guardat correctament");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Establecimiento $establecimiento)
    {
        //
    }
}
