@extends("layouts.app")

@section("styles")
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
crossorigin=""/>

<link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css" integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section("content")
<div class="container">
    <h1 class="text-center mt-4">Editar l'Establiment</h1>

    <div class="mt-5 row justify-content-center">
        <form action="{{ route("establecimiento.update", ["establecimiento" => $establecimiento->id]) }}" method="POST" class="col-md-9 col-xs-12 card card-body" enctype="multipart/form-data">
            @csrf
            @method("put")

            <fieldset class="border p-4">
                <legend class="text-primary">Nom, Categoria i Imatge Principal</legend>

                <div class="form-group">
                    <label for="nombre">Nom del Establiment</label>

                    <input type="text" id="nombre" name="nombre" class="form-control @error("nombre") is-invalid @enderror" placeholder="Nom Establiment" value="{{ $establecimiento->nombre }}">

                    @error("nombre")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="categoria_id">Categoria</label>

                    <select name="categoria_id" id="categoria_id" class="form-control @error("categoria_id") is-invalid @enderror">

                        <option value="" selected disabled>-- Selecciona --</option>
                        @foreach ($categorias as $categoria)
                        <option value="{{ $categoria->id }}" {{ $establecimiento->categoria_id == $categoria->id ? "selected" : "" }}>{{ $categoria->nombre }}</option>
                        @endforeach

                    </select>

                    @error("categoria_id")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="imagen_principal">Imatge Principal</label>

                    <input type="file" id="imagen_principal" name="imagen_principal" class="form-control @error("imagen_principal") is-invalid @enderror">

                    @error("imagen_principal")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror

                    <img src="/storage/{{ $establecimiento->imagen_principal }}" alt="" style="width:200px; margin-top: 20px">
                </div>
            </fieldset>


            <fieldset class="border p-4 mt-5">
                <legend class="text-primary">Ubicació</legend>

                <div class="form-group">
                    <label for="formbuscador">Cercar Direcció</label>

                    <input type="text" id="formbuscador" name="formbuscador" placeholder="Direcció del Establiment" class="form-control">
                    <p class="text-secondary mt-5 mb-3 text-center">Si us plau ajusti el Pin a la posició exacta</p>
                </div>

                <div class="form-group">
                    <div id="mapa" style="height: 400px;"></div>
                </div>

                <p class="informacion">Confirma que els següents camps siguin correctes</p>

                <div class="form-group">
                    <label for="direccion">Direcció</label>

                    <input type="text" id="direccion" name="direccion" class="form-control @error("direccion") is-invalid @enderror" placeholder="Direcció" value="{{ $establecimiento->direccion }}">

                    @error("direccion")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="localidad">Municipi</label>

                    <input type="text" id="localidad" name="localidad" class="form-control @error("localidad") is-invalid @enderror" placeholder="Municipi" value="{{ $establecimiento->localidad }}">

                    @error("localidad")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <input type="hidden" name="lat" id="lat" value="{{ $establecimiento->lat }}">
                <input type="hidden" name="lng" id="lng" value="{{ $establecimiento->lng }}">

            </fieldset>

            <fieldset class="border p-4 mt-5">
                <legend  class="text-primary">Informació Establiment: </legend>
                    <div class="form-group">
                        <label for="nombre">Telèfon</label>
                        <input
                            type="tel"
                            class="form-control @error('telefono')  is-invalid  @enderror"
                            id="telefono"
                            placeholder="Telèfon Establiment"
                            name="telefono"
                            value="{{ $establecimiento->telefono }}"
                        >

                            @error('telefono')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                    </div>



                    <div class="form-group">
                        <label for="nombre">Descripció</label>
                        <textarea
                            class="form-control  @error('descripcion')  is-invalid  @enderror"
                            name="descripcion"
                        >{{ $establecimiento->descripcion }}</textarea>

                            @error('descripcion')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="nombre">Horari d'obertura:</label>
                        <input
                            type="time"
                            class="form-control @error('apertura')  is-invalid  @enderror"
                            id="apertura"
                            name="apertura"
                            value="{{ $establecimiento->apertura }}"
                        >
                        @error('apertura')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="nombre">Horari de tancament:</label>
                        <input
                            type="time"
                            class="form-control @error('cierre')  is-invalid  @enderror"
                            id="cierre"
                            name="cierre"
                            value="{{ $establecimiento->cierre }}"
                        >
                        @error('cierre')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                        @enderror
                    </div>
            </fieldset>

            <fieldset class="border p-4 mt-5">
                <legend  class="text-primary">Galeria d'Imatges: </legend>
                    <div class="form-group">
                        <label for="imagenes">Imatges</label>
                        <div id="dropzone" class="dropzone" name="imagenes"></div>
                    </div>

                    @if(count($imagenes) > 0)
                        @foreach ($imagenes as $imagen)
                            <input type="hidden" class="galeria" value="{{ $imagen->ruta_imagen }}">
                        @endforeach
                    @endif

            </fieldset>

            <input type="hidden" id="uuid" name="uuid" value="{{ $establecimiento->uuid }}">

            <input type="submit" class="btn btn-primary mt-3 d-block" value="Guardar Canvis">
        </form>
    </div>
</div>
@endsection

@section("scripts")
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
crossorigin=""></script>

<script src="https://unpkg.com/esri-leaflet" defer></script>
<script src="https://unpkg.com/esri-leaflet-geocoder" defer></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/min/dropzone.min.js" integrity="sha512-oQq8uth41D+gIH/NJvSJvVB85MFk1eWpMK6glnkg6I7EdMqC1XVkW7RxLheXwmFdG03qScCM7gKS/Cx3FYt7Tg==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
@endsection
