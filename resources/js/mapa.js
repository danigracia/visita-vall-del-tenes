import { makeArray } from 'jquery';
import { OpenStreetMapProvider } from 'leaflet-geosearch';
const provider = new OpenStreetMapProvider();

document.addEventListener("DOMContentLoaded", () => {

    if (!document.querySelector("#mapa")) {
        return;
    }

    const lat = document.querySelector("#lat").value === "" ? 41.6047959 : document.querySelector("#lat").value;
    const lng = document.querySelector("#lng").value === "" ? 2.2883445 : document.querySelector("#lng").value;

    const mapa = L.map('mapa').setView([lat, lng], 14);

    //Eliminar pines previos
    let markers = new L.FeatureGroup().addTo(mapa);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mapa);

    let marker;

    // agregar el pin
    marker = new L.marker([lat, lng], {
        draggable: true,
        autoPan: true
    }).addTo(mapa);

    //Agregar pin a la capa markers
    markers.addLayer(marker);

    reubicarPin(marker);

    //Geocode service
    const geocodeService = L.esri.Geocoding.geocodeService({
        apikey: "AAPKd6b5c8ebd7644c67a8e222d1b5abf0fattIQwGy9lEeUOFKRYW3y3MevbDjmr2s_Fj4iHSqVQ5eWM8desN01l1uOUOaJAiRx"
    });

    //Buscador de direcciones
    const buscador = document.querySelector("#formbuscador");
    buscador.addEventListener('blur', buscarDireccion);

    function reubicarPin(marker) {
        //Detectar movimiento del marker
        marker.on("moveend", function (e) {
            marker = e.target;

            const posicion = marker.getLatLng();

            //Centrar automaticamente
            mapa.panTo(new L.LatLng(posicion.lat, posicion.lng));

            //Reverse Geocoding, cuando el usuario reubica el pin
            geocodeService.reverse().latlng(posicion, 14).run(function (error, resultado) {
                marker.bindPopup(resultado.address.Address);
                marker.openPopup();

                //Llenar inputs
                llenarInputs(resultado)
            });
        });
    }

    function buscarDireccion(e) {
        if (e.target.value.length > 10) {
            provider.search({ query: e.target.value + ", Vallès Oriental, Barcelona, Catalonia" }).then(resultado => {
                if (resultado[0]) {

                    //Limpiar pines previos
                    markers.clearLayers();

                    geocodeService.reverse().latlng(resultado[0].bounds[0], 14).run(function (error, resultado) {
                        //Llenar inputs
                        llenarInputs(resultado);

                        //Centrar mapa
                        mapa.setView(resultado.latlng)

                        //Agregar pin
                        marker = new L.marker(resultado.latlng, {
                            draggable: true,
                            autoPan: true
                        }).addTo(mapa);

                        //Asignar al contenedor de markers el nuevo pin
                        markers.addLayer(marker);

                        reubicarPin(marker);

                    });
                };
            }).catch(error => console.log(error))
        }
    }

    function llenarInputs(resultado) {
        document.querySelector("#direccion").value = resultado.address.Address || "";
        document.querySelector("#localidad").value = resultado.address.City || "";

        document.querySelector("#lat").value = resultado.latlng.lat || "";
        document.querySelector("#lng").value = resultado.latlng.lng || "";
    }
});
